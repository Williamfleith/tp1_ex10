**Readme Ex10:**

Created on 24/04/2020 by William FLEITH


**Objective :** In this exercice we created an account in pypi.org to upload our package online to keep a version of this package and also enable other people to download our package. 

**Upload of our package on pypi :***
- install differents tools with commands :
	- python setup.py bdist_wheel
	- pip install twine pyopenssl ndg-httpsclient pyasn1
- register our package ( you need to be in the right repository and have a pypi account):
	- python setup.py register
- upload your packages :
	- twine upload dist/NomDuPackage-version-py3-none-any.whl

- You can now check if it is online : https://pypi.org/project/TestSimpleCalculatorWilliamfleith/0.0.1/
- To install this package on your own computer you just need to do :
	- pip install TestSimpleCalculatorWilliamfleith
	- pip freeze | grep Test (to see if it has been well installed)





